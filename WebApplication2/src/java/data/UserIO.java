package data;

import java.io.*;
import business.User;

public class UserIO
{
    public static void add(User user, String filepath) 
    throws IOException
    {
        File file = new File(filepath);
        PrintWriter out = new PrintWriter(
                new FileWriter(file, true));
        out.println( user.getMaNV() + "|"
                + user.getHoTen() + "|"
                + user.getPhongBan());        
        out.close();
    }
}
