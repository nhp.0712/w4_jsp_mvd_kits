/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import business.User;
import data.UserIO;
import javax.servlet.ServletContext;

/**
 *
 * @author Admin
 */
public class addEmployee extends HttpServlet {

    int globalCount;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void init() throws ServletException {
        globalCount = 0; // initialize the instance variable
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        globalCount++;
        // get parameters from the request
        String maNV = request.getParameter("maNV");
        String hoTen = request.getParameter("hoTen");
        String phongban = request.getParameter("phongban");

        // get a relative file name
        ServletContext sc = getServletContext();
        String path = sc.getRealPath("/WEB-INF/Employee.txt");

        // use regular Java objects to write the data to a file
        User user = new User(maNV, hoTen, phongban);
        UserIO.add(user, path);

        // send response to browser
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(
                "<!doctype html public \"-//W3C//DTD HTML 4.0 "
                + "    Transitional//EN\">\n"
                + "<html>\n"
                + "<head>\n"
                + "</head>\n"
                + "<body>\n"
                + "\n<p>Here is the information that you entered:</p>\n"
                + "  <table cellspacing=\"5\" cellpadding=\"5\" "
                + "         border=\"1\">\n"
                + "  <tr><td align=\"right\">Ma NV:</td>\n"
                + "      <td>" + maNV + "</td>\n"
                + "  </tr>\n"
                + "  <tr><td align=\"right\">Ho Ten:</td>\n"
                + "      <td>" + hoTen + "</td>\n"
                + "  </tr>\n"
            
                + "  </table>\n"
                + "<p>Phong Ban: </p>\n"
                + "       <td>" + phongban + "</td>\n"
                + "<p>To enter another one, click on the "
                + "Back <br>\n"
                + "button in your browser or the Return button shown <br>\n"
                + "below.</p>\n"
                + "<form action=\"index.html\" "
                + "      method=\"post\">\n"
                + "  <input type=\"submit\" value=\"Return\">\n"
                + "</form>\n" 
                + "</body>\n"
                + "</html>\n");

        out.close();

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
