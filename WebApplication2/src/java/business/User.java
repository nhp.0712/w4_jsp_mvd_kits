package business;

public class User {

    private String maNV;
    private String hoTen;
    private String phongban;

    public User() {
        maNV = "";
        hoTen = "";
        phongban = "";
    }

    public User(String maNV, String hoTen,
           String phongban) {
        this.maNV = maNV;
        this.hoTen = hoTen;
        this.phongban = phongban;

    }

    public void setMaNV(String maNV) {
        this.maNV = maNV;
    }

    public String getMaNV() {
        return maNV;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getHoTen() {
        return hoTen;
    }

   
    
    public void setPhongBan(String maNV) {
        this.phongban = phongban;
    }

    public String getPhongBan() {
        return phongban;
    }
    
}
