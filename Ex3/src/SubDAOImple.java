
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.*;

public class SubDAOImple implements DAO<Subject> {

    private Connection con;
    private DBConnection DBC;
    Subject sub = new Subject();
    Scanner sc = new Scanner(System.in);

    public void add(Subject sub) {
        try {
            con = DBC.getConnect();

            System.out.println("Enter Subject's id: ");
            sub.setID(sc.nextInt());
            sc.nextLine();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ID FROM subject");
            boolean idCheck = true;
            while (rs.next()) {
                if (rs.getInt("ID") == sub.getID()) {
                    System.out.println("That ID already exists in the Database. Try again later!");
                    idCheck = false;
                    break;
                }
            }

            if (idCheck) {
                System.out.println("Enter Subject's name: ");
                sub.setName(sc.nextLine());
                String insert = "INSERT INTO Subjectdetails VALUES (?,?)";

                PreparedStatement stInsert = con.prepareStatement(insert);
                stInsert.setInt(1, sub.getID());
                stInsert.setString(2, sub.getName());

                stInsert.execute();

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void delete() {
        try {
            con = DBC.getConnect();

            System.out.println("Enter Subject's id to delete: ");
            int idDel = sc.nextInt();
            sc.nextLine();
            Statement stmt2 = con.createStatement();
            ResultSet rs2 = stmt2.executeQuery("SELECT ID FROM subject");
            boolean idCheck2 = true;
            while (rs2.next()) {
                if (rs2.getInt("ID") == idDel) {
                    idCheck2 = false;
                    break;
                }
            }
            if (!idCheck2) {
                String delete = "DELETE FROM Subjectdetails WHERE ID = ?";

                PreparedStatement stDelete = con.prepareStatement(delete);
                stDelete.setInt(1, idDel);
                stDelete.execute();
            } else {
                System.out.println("That ID doesn't exists in the Database. Try again later!");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void search(Subject sub) {
        try {
            con = DBC.getConnect();

            System.out.println("Enter Subject's id to search: ");
            int idSearch = sc.nextInt();
            sc.nextLine();

            Statement stmt3 = con.createStatement();
            ResultSet rs3 = stmt3.executeQuery("SELECT ID FROM subject");
            boolean idCheck3 = true;
            while (rs3.next()) {
                if (rs3.getInt("ID") == idSearch) {
                    idCheck3 = false;
                    break;
                }
            }
            if (!idCheck3) {
                String search = "SELECT * FROM subject WHERE ID = ?";
                PreparedStatement stSearch = con.prepareStatement(search);
                stSearch.setInt(1, idSearch);
                ResultSet searchSet = stSearch.executeQuery();
                searchSet.next();
                sub.setID(searchSet.getInt("ID"));
                sub.setName(searchSet.getString("Name"));
                System.out.println("Subject's Information");
                System.out.println("=============================");
                System.out.println(sub.getID() + "| " + sub.getName() + "|  ");
            } else {
                System.out.println("That ID doesn't exists in the Database. Try again later!");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void displayStuScore(Subject t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void displaySubScore(Subject t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
