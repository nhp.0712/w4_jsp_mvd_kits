
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.*;

public class ScoreDAOImple implements DAO<Score> {

    private Connection con;
    private DBConnection DBC;
    Score score = new Score();
    Scanner sc = new Scanner(System.in);

    public void add(Score score) {
        try {
            con = DBC.getConnect();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT studentdetails.Name, subject.Name, score.StudentID, score.SubjectID FROM score" 
                + " INNER JOIN studentdetails ON score.StudentID = studentdetails.ID"
                + " INNER JOIN subject ON subject.ID = score.SubjectID");
            while (rs.next()) {
               System.out.println("Enter Grade for Student" + rs.getString("studentdetails.Name") + " with subject " + rs.getString("subject.Name") + ": ");
               score.setStuID(rs.getInt("score.StudentID"));
               score.setSubID(rs.getInt("score.SubjectID"));
               score.setGrade(sc.nextFloat());
               String update = "UPDATE Score SET Grade = ? WHERE StudentID = ? AND SubjectID = ?";

               PreparedStatement stUpdate = con.prepareStatement(update);
               stUpdate.setFloat(1, score.getGrade());
               stUpdate.setInt(2, score.getStuID());
               stUpdate.setInt(3, score.getSubID());

               stUpdate.execute();
           }
       } 
       catch (SQLException ex) {
        ex.printStackTrace();
    }
}

public void displayStuScore(Score score) {
 try {
    con = DBC.getConnect();
    Statement stmt2 = con.createStatement();
    ResultSet rs2 = stmt2.executeQuery("SELECT studentdetails.Name, score.StudentID, score.Grade FROM score" 
        + " INNER JOIN studentdetails ON score.StudentID = studentdetails.ID"
        + " INNER JOIN subject ON subject.ID = score.SubjectID");
    System.out.println("Score List of All Students");
    System.out.println("=============================");
    while (rs2.next()) {
        score.setStuID(rs2.getInt("score.StudentID"));
        score.setGrade(rs2.getFloat("score.Grade"));
        System.out.println(score.getStuID() + "| " + rs2.getString("studentdetails.Name") + "|    " + score.getGrade() + "   |");

    }
} catch (SQLException ex) {
    ex.printStackTrace();
}
}

public void displaySubScore(Score score) {
    try {
        con = DBC.getConnect();
        Statement stmt3 = con.createStatement();
        ResultSet rs3 = stmt3.executeQuery("SELECT subject.Name, score.SubjectID, score.Grade FROM score" 
            + " INNER JOIN studentdetails ON score.StudentID = studentdetails.ID"
            + " INNER JOIN subject ON subject.ID = score.SubjectID");
        System.out.println("Score List of All Subjects");
        System.out.println("=============================");
        while (rs3.next()) {
            score.setStuID(rs3.getInt("score.SubjectID"));
            score.setGrade(rs3.getFloat("score.Grade"));
            System.out.println(score.getStuID() + "| " + rs3.getString("subject.Name") + "|    " + score.getGrade() + "   |");

        }
    } catch (SQLException ex) {
        ex.printStackTrace();
    }
}

    @Override
    public void delete() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void search(Score t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
