import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.*;

public class DBConnection{
	public static 
        Connection getConnect(){
		String url = "jdbc:mysql://localhost:3306/java?user=root&password=root"
		+ "&autoReconnect=true"
		+ "&useSSL=false"
		+ "&characterEnconding=utf8"
		+ "&useUnicode=true"
		+ "&serverTimezone=UTC";
		Connection con = null;
		try {
			if (con == null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				con = DriverManager.getConnection(url);
			}
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return con;
	}
}