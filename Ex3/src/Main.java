
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.*;

public class Main {

    public static void main(String[] args) throws SQLException {
        StuDAOImple STDI = new StuDAOImple();
        SubDAOImple SUDI = new SubDAOImple();
        ScoreDAOImple SCDI = new ScoreDAOImple();
        int choice = 0;
        while (choice != 10) {
            Student stu = new Student();
            Subject sub = new Subject();
            Score score = new Score();
            Scanner sc = new Scanner(System.in);
            System.out.println("Menu: ");
            System.out.println("==================");
            System.out.println("1. Add Student ");
            System.out.println("2. Delete Student ");
            System.out.println("3. Search Student ");
            System.out.println("4. Add Subject ");
            System.out.println("5. Delete Subject ");
            System.out.println("6. Search Subject ");
            System.out.println("7. Scoring an Exam for all Students ");
            System.out.println("8. Print Score of all Students ");
            System.out.println("9. Print Score of all Subjects ");
            System.out.println("10. Exit ");
            System.out.println("Your choice: ");

            choice = sc.nextInt();
            sc.nextLine();
            switch (choice) {
                case 1:
                    STDI.add(stu);
                    break;

                case 2:
                    STDI.delete();
                    break;
                case 3:
                    STDI.search(stu);
                    break;

                case 4:
                    SUDI.add(sub);
                    break;

                case 5:
                    SUDI.delete();
                    break;
                case 6:
                    SUDI.search(sub);
                    break;

                case 7:
                    SCDI.add(score);
                    break;

                case 8:
                    SCDI.displayStuScore(score);
                    break;

                case 9:
                    SCDI.displaySubScore(score);
                    break;

                case 10:
                    break;
            }
        }

    }
}
