import java.sql.Connection;  
import java.sql.DriverManager;  
import java.sql.*;

public class Ex35{
	public static void main(String[] args){
		try{
			Class.forName("com.mysql.cj.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/java?serverTimezone=UTC","root","root");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from student");
			while(rs.next()){
				System.out.println(rs.getInt("ID") + "|" + rs.getString("FirstName") + "|" + rs.getString("LastName"));
			}

			con.close();
			
		}
		catch(ClassNotFoundException ex){
			ex.printStackTrace();
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
	}
}