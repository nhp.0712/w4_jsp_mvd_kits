
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.*;

public class Ex1 {

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://localhost:3306/java?user=root&password=root"
                + "&autoReconnect=true"
                + "&useSSL=false"
                + "&characterEnconding=utf8"
                + "&useUnicode=true"
                + "&serverTimezone=UTC";
        Connection con = null;
        try {
            if (con == null) {
                Class.forName("com.mysql.cj.jdbc.Driver");
                con = DriverManager.getConnection(url);
            }
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        int choice = 0;
        while (choice != 4) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Menu: ");
            System.out.println("==================");
            System.out.println("1. Add Student ");
            System.out.println("2. Delete Student ");
            System.out.println("3. Search Student ");
            System.out.println("4. Exit ");
            System.out.println("Your choice: ");

            choice = sc.nextInt();
            sc.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("Enter student's id: ");
                    int id = sc.nextInt();
                    sc.nextLine();
                    Statement stmt = con.createStatement();
                    ResultSet rs = stmt.executeQuery("SELECT ID FROM studentdetails");
                    boolean idCheck = true;
                    while (rs.next()) {
                        if (rs.getInt("ID") == id) {
                            System.out.println("That ID already exists in the Database. Try again later!");
                            idCheck = false;
                            break;
                        }
                    }

                    if (idCheck) {
                        System.out.println("Enter student's name: ");
                        String name = sc.nextLine();
                        System.out.println("Enter student's class name: ");
                        String classname = sc.nextLine();
                        String insert = "INSERT INTO studentdetails VALUES (?,?,?)";

                        PreparedStatement stInsert = con.prepareStatement(insert);
                        stInsert.setInt(1, id);
                        stInsert.setString(2, name);
                        stInsert.setString(3, classname);

                        stInsert.execute();

                    }
                    break;

                case 2:
                    System.out.println("Enter student's id to delete: ");
                    int idDel = sc.nextInt();
                    sc.nextLine();
                    Statement stmt2 = con.createStatement();
                    ResultSet rs2 = stmt2.executeQuery("SELECT ID FROM studentdetails");
                    boolean idCheck2 = true;
                    while (rs2.next()) {
                        if (rs2.getInt("ID") == idDel) {
                            idCheck2 = false;
                            break;
                        }
                    }
                    if (!idCheck2) {
                        String delete = "DELETE FROM studentdetails WHERE ID = ?";

                        PreparedStatement stDelete = con.prepareStatement(delete);
                        stDelete.setInt(1, idDel);
                        stDelete.execute();
                        break;
                    } else {
                        System.out.println("That ID doesn't exists in the Database. Try again later!");
                    }

                case 3:
                    System.out.println("Enter student's id to search: ");
                    int idSearch = sc.nextInt();
                    sc.nextLine();

                    Statement stmt3 = con.createStatement();
                    ResultSet rs3 = stmt3.executeQuery("SELECT ID FROM studentdetails");
                    boolean idCheck3 = true;
                    while (rs3.next()) {
                        if (rs3.getInt("ID") == idSearch) {
                            idCheck3 = false;
                            break;
                        }
                    }
                    if (!idCheck3) {
                        String search = "SELECT * FROM studentdetails WHERE ID = ?";
                        PreparedStatement stSearch = con.prepareStatement(search);
                        stSearch.setInt(1, idSearch);
                        ResultSet searchSet = stSearch.executeQuery();
                        searchSet.next();
                        System.out.println("Student's Information");
                        System.out.println("=============================");
                        System.out.println(searchSet.getInt("ID") + "| " + searchSet.getString("Name") + "|  " + searchSet.getString("Class_Name"));
                    } else {
                        System.out.println("That ID doesn't exists in the Database. Try again later!");
                    }
                    break;
            }
        }
        con.close();

    }
}
