
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.*;

public class Ex2 {

    public static void main(String[] args) throws SQLException {
        Func f = new Func();
        int choice = 0;
        while (choice != 4) {
            Student stu = new Student();
            Scanner sc = new Scanner(System.in);
            System.out.println("Menu: ");
            System.out.println("==================");
            System.out.println("1. Add Student ");
            System.out.println("2. Delete Student ");
            System.out.println("3. Search Student ");
            System.out.println("4. Exit ");
            System.out.println("Your choice: ");

            choice = sc.nextInt();
            sc.nextLine();
            switch (choice) {
                case 1:
                    f.add(stu);
                    break;

                case 2:
                    f.delete();

                case 3:
                    f.search(stu);
                    break;

                case 4:
                    break;
            }
        }

    }
}
