
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;
import java.sql.*;

public class Func implements Functioning<Student> {

    private Connection con;
    private DBConnection DBC;
    Student stu = new Student();
    Scanner sc = new Scanner(System.in);

    public void add(Student stu) {
        try {
            con = DBC.getConnect();

            System.out.println("Enter student's id: ");
            stu.setID(sc.nextInt());
            sc.nextLine();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT ID FROM studentdetails");
            boolean idCheck = true;
            while (rs.next()) {
                if (rs.getInt("ID") == stu.getID()) {
                    System.out.println("That ID already exists in the Database. Try again later!");
                    idCheck = false;
                    break;
                }
            }

            if (idCheck) {
                System.out.println("Enter student's name: ");
                stu.setName(sc.nextLine());
                System.out.println("Enter student's class name: ");
                stu.setClassName(sc.nextLine());
                String insert = "INSERT INTO studentdetails VALUES (?,?,?)";

                PreparedStatement stInsert = con.prepareStatement(insert);
                stInsert.setInt(1, stu.getID());
                stInsert.setString(2, stu.getName());
                stInsert.setString(3, stu.getClassName());

                stInsert.execute();

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void delete() {
        try {
            con = DBC.getConnect();

            System.out.println("Enter student's id to delete: ");
            int idDel = sc.nextInt();
            sc.nextLine();
            Statement stmt2 = con.createStatement();
            ResultSet rs2 = stmt2.executeQuery("SELECT ID FROM studentdetails");
            boolean idCheck2 = true;
            while (rs2.next()) {
                if (rs2.getInt("ID") == idDel) {
                    idCheck2 = false;
                    break;
                }
            }
            if (!idCheck2) {
                String delete = "DELETE FROM studentdetails WHERE ID = ?";

                PreparedStatement stDelete = con.prepareStatement(delete);
                stDelete.setInt(1, idDel);
                stDelete.execute();
            } else {
                System.out.println("That ID doesn't exists in the Database. Try again later!");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void search(Student stu) {
        try {
            con = DBC.getConnect();

            System.out.println("Enter student's id to search: ");
            int idSearch = sc.nextInt();
            sc.nextLine();

            Statement stmt3 = con.createStatement();
            ResultSet rs3 = stmt3.executeQuery("SELECT ID FROM studentdetails");
            boolean idCheck3 = true;
            while (rs3.next()) {
                if (rs3.getInt("ID") == idSearch) {
                    idCheck3 = false;
                    break;
                }
            }
            if (!idCheck3) {
                String search = "SELECT * FROM studentdetails WHERE ID = ?";
                PreparedStatement stSearch = con.prepareStatement(search);
                stSearch.setInt(1, idSearch);
                ResultSet searchSet = stSearch.executeQuery();
                searchSet.next();
                stu.setID(searchSet.getInt("ID"));
                stu.setName(searchSet.getString("Name"));
                stu.setClassName(searchSet.getString("Class_Name"));
                System.out.println("Student's Information");
                System.out.println("=============================");
                System.out.println(stu.getID() + "| " + stu.getName() + "|  " + stu.getClassName());
            } else {
                System.out.println("That ID doesn't exists in the Database. Try again later!");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
