public interface Functioning<T>{
	public void add(T t);
	public void delete();
	public void search(T t);
}